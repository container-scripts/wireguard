# WireGuard Usecases

Wireguard is quite flexible and can be used in many situations.
However we will describe and discuss only those cases that are
supported by these scripts (which hopefully are among the most common
ones).

First of all, these scripts are meant to install WireGuard on a server
with a public IP, and to generate and offer configurations to clients
that don't have a public IP (are behind NAT).

## Usecase #1: Secure connection to the Internet

This is maybe the most popular reason why people want to use a VPN.
If you are connected to an unknown WiFi hotspot (one that it is not
under your control), it is quite possible that someone may try to
eavesdrop your communications, and maybe try to hack you. The
recommended solution is to use a VPN for connecting to the Internet.

![](img/vpn-tunnel.png)

For this case the settings should be like this:

```bash
ROUTED_NETWORKS="0.0.0.0/0"
DNS_SERVERS="176.103.130.130, 176.103.130.131"
ALLOW_INTERNET_ACCESS=yes
CLIENT_TO_CLIENT=no
KEEPALIVE_PERIOD=0
```

By the way, these are the default values on `settings.sh`.

Let's discuss their meaning:

1. The setting `ROUTED_NETWORKS="0.0.0.0/0"` tells the client to route
   everything to the wireguard interface (`wg0`). So, all the internet
   traffic will go through the WG tunnel.

1. `DNS_SERVERS="176.103.130.130, 176.103.130.131"` tells the client
   which DNS servers to use. You can use your preferred DNS servers
   here.  The client can also change his configuration file and
   customize the DNS servers.

1. When `ALLOW_INTERNET_ACCESS=yes` then these firewall rules will be
   added on the server:
   
   ```bash
   iptables -I FORWARD -i wg0 -j ACCEPT
   iptables -t nat -I POSTROUTING -o eth0 -j MASQUERADE
   ```
   
   This allows the WG server to behave as a NAT server for the
   clients, providing them access to internet.
   
1. The setting `CLIENT_TO_CLIENT=no` tells the server to block the
   connections between the clients. This is achieved with a firewall
   rule like this:
   
   ```bash
   iptables -I FORWARD -i wg0 -o wg0 -j DROP
   ```
   
   This is what you usually want when using VPN for secure access to
   the Internet. However if all the clients belong to you, and you
   would prefer them to be able to access each-other, then change it
   to `CLIENT_TO_CLIENT=yes`.

1. The setting `KEEPALIVE_PERIOD` makes the client to send periodically
   a packet to the server.
   
   WG by default sends data only when there is anything to send. When
   client sends no data for some time, the NAT (behind which the
   client is) terminates the session, and the connection to the server
   (and to the other clients) is lost. Usually this is fine, because
   the connection will be established again when the client needs to
   access the internet. So, the setting `KEEPALIVE_PERIOD=0` (which
   disables the *keepalive* feature) is OK.
   
   However, if the clients want to communicate with each-other
   (setting `CLIENT_TO_CLIENT=yes` above), it is important that each
   client keeps the connection to the server alive, so that it can be
   reached by the other clients when they want to access it. In this
   case, `25` seconds is a good value for `KEEPALIVE_PERIOD`.

### Usecase #1: Test cases: [Test #1](/testing#test-1-accessing-the-internet-from-a-wg-client), [Test #2](/testing#test-2-two-clients-cannot-ping-each-other), [Test #3](/testing#test-3-two-clients-can-ping-each-other)

## Usecase #2: Virtual Private LAN

In this case there are several computers distributed all over the
internet, and we want them to communicate with each-other safely and
securely, as if they are in a private LAN.

![WG Virtual Private LAN](img/vpn-lan.svg)

![WG Virtual Private LAN](img/vpn-lan-1.svg)

This is an important usecase that facilitates remote collaboration of
distributed teams, remote education, etc.

The settings for this case should be like this:

```bash
ROUTED_NETWORKS="192.168.10.0/24"
ALLOW_INTERNET_ACCESS=no
CLIENT_TO_CLIENT=yes
KEEPALIVE_PERIOD=25
```

1. The first setting will tell the clients to route the traffic for
   `192.168.10.0/24` through the WG interface. All the rest of the
   traffic will go through the normal gateway.
   
1. The setting `ALLOW_INTERNET_ACCESS=no` tells the server to block
   the internet access from the clients. So, even if the clients
   change manually the first setting on their configuration to
   `AllowedIps = 0.0.0.0/0`, trying to route everything through the WG
   interface, they still will not be able to have internet access.
   
1. The third setting makes sure that the clients can `ping` and access
   each-other (otherwise it wouldn't be a virtual LAN).
   
1. The last setting tells the clients to send a *keep-connection-alive*
   package every 25 seconds. This prevents the NAT sessions from expiring
   and makes sure that each client can be accessed from the other clients
   at any time.

### Usecase #2: Test cases: [Test #4](/testing#test-4-virtual-private-lan)

## Usecase #3: Routing between remote private LANs

This case is an extension of the second case above, in the sense that
not only the WG clients can communicate with each other, but also the
other clients on their LANs can communicate with each other.

![Routing between remote private LANs](img/routing.svg)

For example from Client3 and Client4 we can ping to Client5 and
Client6 (and the other way around), with the network traffic being
routed safely through WG clients and the WG server.

This usecase might be useful for companies that have branches on
remote locations. When the number of computers to be connected is big
and dynamic (new computers may be added frequently), routing between
two LANs is more practical than making each computer a WG client.

The steps to setup this case are these:

1. Edit `settings.sh` like this:

   ```bash
   ROUTED_NETWORKS="192.168.11.0/24, 192.168.12.0/24"
   ALLOW_INTERNET_ACCESS=no
   CLIENT_TO_CLIENT=yes
   KEEPALIVE_PERIOD=25
   ```

   `ROUTED_NETWORKS` should contain at least all the LANs that will be
   connected, but you may also add `10.100.100.1/32` (the IP of the WG
   server), `192.168.10.0/24` (the network of the WG clients), etc.

1. When adding client configurations on the server, give also the LAN
   that is going to use this client as a gateway, like this:

   ```bash
   cs client add client1 192.168.10.1 192.168.11.0/24
   cs client add client2 192.168.10.2 192.168.12.0/24
   ```

   This will tell the WG server to allow the traffic from these
   networks.

1. We also need to add proper routes to Client3, Client4, Client5,
   Client6, etc. For example, on Client3 we should add this route:

   ```bash
   ip route add to 192.168.12.0/24 via 192.168.11.2
   ```

   On Client5 we should add a route like this:

   ```bash
   ip route add to 192.168.11.0/24 via 192.168.12.2
   ```

### Usecase #3: A simpler setup

Another way, a bit more convenient, might be to use the command `cs
setup router`, like this:

```bash
cs setup router \
    client1:192.168.10.1:192.168.11.0/24 \
    client2:192.168.10.2:192.168.12.0/24
```

This will both modify `settings.sh` with the right value of
`ROUTED_NETWORKS` (and other proper settings), and will create the
client configurations as above.

However you still need to add the routes to the non-WG clients, as
shown above.

### Usecase #3: An alternative configuration

If we want all the clients to access the internet through the WG
server (besides accessing each other), then the setup should be like
this:

1. Edit `settings.sh` like this:

   ```bash
   ROUTED_NETWORKS="0.0.0.0/0"
   ALLOW_INTERNET_ACCESS=yes
   CLIENT_TO_CLIENT=yes
   KEEPALIVE_PERIOD=25
   ```

1. Create client configurations like this:

   ```bash
   cs client add client1 192.168.10.1 192.168.11.0/24
   cs client add client2 192.168.10.2 192.168.12.0/24
   ```

1. Add default routes to clients, like this:

   ```bash
   # on Client3 and Client4
   ip route add to default via 192.168.11.2

   # on Client5 and Client6
   ip route add to default via 192.168.12.2
   ```

### Usecase #3: Test cases: [Test #5](/testing#test-5-routing-between-two-lans)

## Usecase #4: Accessing clients from a cloud server

In this case we want the clients and a containerized application on
the cloud to be able to access each-other, but the clients themselves
should not be able to access each-other. This situation is represented
on the diagram:

![Accessing clients from a cloud server](img/accessing-clients.svg)

This case might be useful if we have for example a containerized
Guacamole server (on Client3) and want to access from it some machines
that are located on private networks (Client1 and Client2).

The steps to setup this case are these:

1. Edit `settings.sh` like this:

   ```bash
   ROUTED_NETWORKS="172.24.0.0/16"
   ALLOW_INTERNET_ACCESS=no
   CLIENT_TO_CLIENT=no
   KEEPALIVE_PERIOD=25
   ```

   `ROUTED_NETWORKS` contains the podman LAN where the WG container
   and the Guacamole container are located.

1. Create client configurations on the server:

   ```bash
   cs client add client1 192.168.10.1
   cs client add client2 192.168.10.2
   ```

1. So far Client1 and Client2 can ping to Client3 (`172.24.0.3`), but
   Client3 cannot ping back to them (to `192.168.10.1` and to
   `192.168.10.2`). In order to fix this, add this route to Client3:

   ```bash
   ip route add to 192.168.10.0/24 via 172.24.0.2
   ```

Note also that Client1 and Client2 cannot ping to each-other or the WG
server, and this is ensured by the setting `CLIENT_TO_CLIENT=no`.

### Usecase #4: Accessig LANs as well from a cloud server

Sometimes it may be more convenient to access from the server the LANs
behind the WG clients as well. For example in the following diagram we
want Client3 and Client4 to be able to access each other:

![Accessing LANs from a cloud server](img/accessing-clients-lan.svg)

The setup for this case is almost the same as with the previos case,
with these small modifications:

1. When creating clients we also give the LANs that should be allowed
   by the WG server, like this:
   
   ```bash
   cs client add client1 192.168.10.1 192.168.11.0/24
   cs client add client2 192.168.10.2 192.168.12.0/24
   ```

1. On Client3 we should add these routes as well:

   ```bash
   ip route add to 192.168.11.0/24 via 172.24.0.2
   ip route add to 192.168.12.0/24 via 172.24.0.2
   ```

1. On Client4 (and similarly to the other LAN clients) we should add a
   route like this:
   
   ```bash
   ip route add to 172.24.0.0/16 via 192.168.11.2
   ```

### Usecase #4: Test cases: [Test #6](/testing#test-6-accessing-clients-from-a-cloud-server)
