#!/bin/bash

### create an apache2 config
cat <<EOF >> /etc/apache2/conf-available/clients.conf
<Directory /var/www/html/clients>
    Options -Indexes
</Directory>
EOF

### enable the config and restart apache2
mkdir -p /host/www
ln -s /host/www /var/www/html/clients
a2enconf clients
