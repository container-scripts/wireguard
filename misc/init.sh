#!/bin/bash

# copy wg0.sh
cp $APPS/wireguard/misc/wg0.sh .
chmod +x wg0.sh

# customize settings.sh
random_port=$(shuf -i49152-65535 -n1)
sed -i settings.sh \
    -e "/^WG_PORT=/ c WG_PORT=$random_port"
