APP=wireguard

### Docker settings
IMAGE=wireguard
CONTAINER=wireguard
NETWORK=wgnet

### Forwarded ports
WG_PORT=51820
HTTPS_PORT=10443   # for sharing client config files

### Clients route these networks through the WG interface.
ROUTED_NETWORKS="0.0.0.0/0"

### Clients use these servers for resolving DNS queries.
DNS_SERVERS="176.103.130.130, 176.103.130.131"

### Server provides internet access to clients through NAT.
ALLOW_INTERNET_ACCESS=yes

### Server allows clients to communicate with each-other.
#CLIENT_TO_CLIENT=yes

### Clients send a keep-connection-alive package periodically.
#KEEPALIVE_PERIOD=25
