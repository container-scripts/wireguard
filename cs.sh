get_public_ip() {
    local services='ifconfig.co ifconfig.me icanhazip.com'
    local pattern='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$'
    local ip
    for service in $services; do
        ip=$(curl -s $service)
        [[ $ip =~ $pattern ]] && echo $ip && return
    done
    return 1
}
