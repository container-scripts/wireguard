#!/bin/bash -i

TITLE="Two clients cannot ping each-other"
DESCRIPTION="
This test is related to the usecase#1, where a WG client can
access the internet through a secure connection to the WG server.
By default, two different clients cannot ping each-other.
The setting CLIENT_TO_CLIENT by default is commented out
(which means disabled).
"
NETWORKS="
    WG:172.24.0.0/16
    LAN1:172.25.0.0/16
    LAN2:172.26.0.0/16
"
CLIENTS="
    LAN1:client1:192.168.100.1
    LAN2:client2:192.168.100.2
"

source $(dirname $0)/lib.sh

#
# Step 1
# Check that CLIENT_TO_CLIENT on the server settings.sh
# is disabled (commented out) by default.
#
##

:; cd $TEST_DIR/networks/WG/server/

:; grep CLIENT_TO_CLIENT settings.sh

:; hl_bash settings.sh

#
# Step 2
# Create config files for each client and for the server.
#
##

:; setup_config_files    # create config files for each client

:; setup_local_endpoint    # set Endpoint to the local ip of the server

:; cd $TEST_DIR/networks/

:; hl_conf LAN1/client1/wg0.conf    # client1 config

:; hl_conf LAN2/client2/wg0.conf    # client2 config

:; hl_conf WG/server/wg0.conf    # server config

#
# Step 3
# Start up the interface wg0 on LAN1/client1
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec wg-quick up wg0

:; cs exec ip addr

:; cs exec ping -c3 10.100.100.1

:; cs exec traceroute 8.8.8.8

#
# Step 4
# Start up the interface wg0 on LAN2/client2
#
##

:; cd $TEST_DIR/networks/LAN2/client2/

:; cs exec wg-quick up wg0

:; cs exec ip addr

:; cs exec ping -c3 10.100.100.1

:; cs exec traceroute 8.8.8.8

#
# Step 5
# Try to ping from client1 to client2
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec ping -c3 192.168.100.2

#
# Step 5
# Try to ping from client2 to client1
#
##

:; cd $TEST_DIR/networks/LAN2/client2/

:; cs exec ping -c3 192.168.100.1

:; cleanup_test_environment    # End
