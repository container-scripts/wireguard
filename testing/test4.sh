#!/bin/bash -i

TITLE="Virtual private LAN"
DESCRIPTION="
This test is related to the usecase#2, where clients can access
each-other, but cannot access the internet through the WG interface
(because of the setting 'ALLOW_INTERNET_ACCESS=no').
"
NETWORKS="
    WG:172.24.0.0/16
    LAN1:172.25.0.0/16
    LAN2:172.26.0.0/16
"
CLIENTS="
    LAN1:client1:192.168.100.1
    LAN2:client2:192.168.100.2
"

source $(dirname $0)/lib.sh

#
# Step 1
# Make sure that ROUTED_NETWORKS is set to '10.100.100.1, 192.168.100.0/24',
# on the server settings, ALLOW_INTERNET_ACCESS=no, CLIENT_TO_CLIENT=yes, 
# and KEEPALIVE_PERIOD=25.
#
##

:; cd $TEST_DIR/networks/WG/server/

:; sed -i settings.sh -e '/ROUTED_NETWORKS/ c ROUTED_NETWORKS="10.100.100.1, 192.168.100.0/24"'

:; sed -i settings.sh -e '/ALLOW_INTERNET_ACCESS/ c ALLOW_INTERNET_ACCESS=no'

:; sed -i settings.sh -e '/CLIENT_TO_CLIENT/ c CLIENT_TO_CLIENT=yes'

:; sed -i settings.sh -e '/KEEPALIVE_PERIOD/ c KEEPALIVE_PERIOD=25'

:; hl_bash settings.sh

#
# Step 2
# Generate client configurations and copy them to client1 and client2.
#
##

:; setup_config_files

:; setup_local_endpoint

#
# Step 3
# Start up wg0 on client1 and check that the client
# does not access the internet through the WG server.
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec wg-quick up wg0

:; cs exec ip addr

:; cs exec traceroute 8.8.8.8

: "It should not access the internet using 10.100.100.1 as a gateway"

#
# Step 4
# Do the same thing for LAN2/client2
#
##

:; cd $TEST_DIR/networks/LAN2/client2/

:; cs exec wg-quick up wg0

:; cs exec ip addr

:; cs exec traceroute 8.8.8.8

#
# Step 5
# Ping from client1 to client2
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec ping -c3 192.168.100.2

#
# Step 6
# Ping from client2 to client1
#
##

:; cd $TEST_DIR/networks/LAN2/client2/

:; cs exec ping -c3 192.168.100.1

#
# Step 7
# What if we set "AllowedIPs = 0.0.0.0/0" on wg0.conf? Will it be able
# to access the internet through the WG interface? Let's try it.
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec wg-quick down wg0

:; sed -i wg0.conf -e '/AllowedIPs/ c AllowedIPs = 0.0.0.0/0'

:; grep -E '^|AllowedIPs.*' wg0.conf

:; cs exec wg-quick up wg0

:; cs exec ping -c3 192.168.100.2    # check that it still can ping to client2

:; cs exec ping -c3 8.8.8.8    # check that it cannot ping to 8.8.8.8

:; cs exec traceroute -m5 8.8.8.8

: "The setting 'ALLOW_INTERNET_ACCESS=no' on the server"
: "does not allow the clients to reach the internet."

:; cleanup_test_environment    # End
