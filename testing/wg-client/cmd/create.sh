# Extend the base create command with additional options.
rename_function cmd_create base_cmd_create
cmd_create() {
    base_cmd_create \
        --cap-add net_admin \
        --cap-add sys_module \
        --sysctl net.ipv4.conf.all.src_valid_mark=1 \
	"$@"
}
