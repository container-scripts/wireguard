#!/bin/bash -i

TITLE="Two clients can ping each-other"
DESCRIPTION="
This test is related to a customized version of usecase#1,
where WG clients can also ping each-other, besides accessing
the internet through the WG server.
This requires the setting 'CLIENT_TO_CLIENT=yes' (enabled).
"
NETWORKS="
    WG:172.24.0.0/16
    LAN1:172.25.0.0/16
    LAN2:172.26.0.0/16
"
CLIENTS="
    LAN1:client1:192.168.100.1
    LAN2:client2:192.168.100.2
"

source $(dirname $0)/lib.sh

#
# Step 1
# Make sure that CLIENT_TO_CLIENT on the server settings.sh
# is set to 'yes'.
#
##

:; cd $TEST_DIR/networks/WG/server/

:; sed -i settings.sh -e '/CLIENT_TO_CLIENT/ c CLIENT_TO_CLIENT=yes'

:; grep CLIENT_TO_CLIENT settings.sh

:; hl_bash settings.sh

#
# Step 2
# Generate client configurations and copy them to client1 and client2.
#
##

:; setup_config_files

:; setup_local_endpoint

#
# Step 3
# Start up the interface wg0 on client1
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec wg-quick up wg0

:; cs exec ip addr

:; cs exec ping -c3 10.100.100.1

:; cs exec traceroute 8.8.8.8

#
# Step 4
# Start up the interface wg0 on client2
#
##

:; cd $TEST_DIR/networks/LAN2/client2/

:; cs exec wg-quick up wg0

:; cs exec ip addr

:; cs exec ping -c3 10.100.100.1

:; cs exec traceroute 8.8.8.8

#
# Step 5
# Ping from client1 to client2
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec ping -c3 192.168.100.2

#
# Step 6
# Ping from client2 to client1
#
##

:; cd $TEST_DIR/networks/LAN2/client2/

:; cs exec ping -c3 192.168.100.1

:; cleanup_test_environment    # End
