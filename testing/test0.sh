#!/bin/bash -i

TITLE="Template file for a test script."
DESCRIPTION="
This test file is a template file that shows the general structure
of a test script, 
"
NETWORKS="
    WG:172.24.0.0/16
    LAN1:172.25.0.0/16
"
CLIENTS="
    LAN1:client1:192.168.100.1
"

source $(dirname $0)/lib.sh

#
# Fix 'settings.sh' on WG/server.
#
##

:; cd $TEST_DIR/networks/WG/server/

:; hl_bash settings.sh

:; cs restart

#
# Create config files for each client and for the server.
#
##

:; setup_config_files    # create config files for each client

:; setup_local_endpoint    # set Endpoint to the local ip of the server

:; hl_conf wg0.conf    # show configuration of the server

#
# Go to client1
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; ls

:; hl_conf wg0.conf    # show configuration of the client

:; cs exec addr

:; cs exec route

#
# Destroy the test environment
#
##

:; cleanup_test_environment    # End
