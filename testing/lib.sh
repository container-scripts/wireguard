#!/bin/bash

help() {
    cat <<EOF
Functions:
    initialize_test_environment
        Build the test networks and containers,
        as defined on the variables NETWORKS and CLIENTS.

    cleanup_test_environment
        Delete any test containers and networks.

    stop_containers
        Stop all the test containers.

    start_containers
        Start all the test containers.

    setup_config_files
        Create WG config files for each client.

    setup_local_endpoint
        Set the endpoint of each client to the local container network IP
        of the WG server (something like 172.24.0.1). This allows testing
        on a machine that does not have a public IP.

    hl_bash [<file.sh>]
    hl_conf [<file.conf>]
        Display a bash or a config file highlighted.
EOF
}

[[ $(basename $0) == 'lib.sh' ]] && help && exit

# get the directory of testing in a variable
TEST_DIR=$(dirname $(realpath $0))
cd $TEST_DIR

# restart the script with './play'
# unless the first option is 'noplay'
case $1 in
    noplay|playing)
        shift
        ;;
    auto|autoplay)
        shift
        exec env DELAY=1.5 ./play $0 playing "$@"
        ;;
    *)
        shift
        exec ./play $0 playing "$@"
        ;;
esac

initialize_test_environment() {
    cleanup_test_environment

    # create an assoc array for the subnets
    declare -gA SUBNETS
    local item network subnet
    for item in $NETWORKS; do
        IFS=: read network subnet <<< $item
        SUBNETS[$network]=$subnet
    done

    # get the name of the network of the WG server
    WGNET=$(echo $NETWORKS | cut -d' ' -f1 | cut -d: -f1)

    create_wg_test_server
    create_wg_test_clients
}

create_wg_test_server() {
    # get the scripts of the server
    cs pull wireguard

    # create a directory for the container
    cd $TEST_DIR
    local dir=networks/$WGNET/server
    mkdir -p $dir
    cd $dir

    # initialize settings on this directory
    cs init wireguard

    # change CONTAINER, NETWORK and SUBNET on settings.sh
    sed -i settings.sh -e "/^CONTAINER=/ c CONTAINER=wg-test-server"
    sed -i settings.sh -e "/^NETWORK=/ c NETWORK=wg-test-$WGNET"
    sed -i settings.sh -e "/^SUBNET=/ c SUBNET=${SUBNETS[$WGNET]}"

    # build the container
    cs make
}

create_wg_test_clients() {
    # copy the scripts of wg-client
    rm -rf /opt/container-scripts/wg-client/
    cp -a $TEST_DIR/wg-client /opt/container-scripts/

    local item network subnet client _rest
    for item in $CLIENTS; do
        IFS=: read network client _rest <<< $item

        # create a directory for the client container
        cd $TEST_DIR
        dir=networks/$network/$client
        mkdir -p $dir
        cd $dir

        # initialize
        cs init wg-client

        # change CONTAINER, NETWORK and SUBNET on settings.sh
        subnet=${SUBNETS[$network]}
        sed -i settings.sh \
            -e "/^CONTAINER=/ c CONTAINER=wg-test-$client" \
            -e "/^NETWORK=/ c NETWORK=wg-test-$network" \
            -e "/^SUBNET=/ c SUBNET='$subnet'"

        # build the container
        cs make
    done
}

cleanup_test_environment() {
    # remove containers
    podman ps --format '{{.Names}}' \
        | grep wg-test- \
        | xargs podman rm -f 2>/dev/null

    # remove networks
    podman network ls --format '{{.Name}}' \
        | grep wg-test- \
        | xargs podman network rm 2>/dev/null

    # remove images
    #podman rmi wg-client wireguard

    # remove directories
    rm -rf /opt/container-scripts/wg-client
    rm -rf $TEST_DIR/networks
}

stop_containers() {
    # stop clients
    local item network client _rest
    for item in $CLIENTS; do
        IFS=: read network client _rest <<< $item
        cd $TEST_DIR/networks/$network/$client
        cs stop
    done

    # stop the server
    cd $TEST_DIR/networks/$WGNET/server
    cs stop
}

start_containers() {
    # stop clients
    local item network client _rest
    for item in $CLIENTS; do
        IFS=: read network client _rest <<< $item
        cd $TEST_DIR/networks/$network/$client
        cs start
    done

    # stop the server
    cd $TEST_DIR/networks/$WGNET/server
    cs start
}

setup_config_files() {
    # go to the directory of the server container
    cd $TEST_DIR/networks/$WGNET/server

    # create a configuration file for each client
    local item network client ip is_gateway lan
    for item in $CLIENTS; do
        IFS=: read network client ip is_gateway <<< $item
        [[ -z $ip ]] && continue
        lan='' ; [[ -n $is_gateway ]] && lan=${SUBNETS[$network]}

        cs client del $client
        cs client add $client $ip $lan >/dev/null

        # copy config file to the directory of the client container
        cp clients/$client.conf $TEST_DIR/networks/$network/$client/wg0.conf
    done

    # restart the server
    cs restart
}

setup_local_endpoint() {
    local subnet=${SUBNETS[$WGNET]}
    local wg_server_ip=${subnet%.*}.1

    local item network client ip is_gateway
    cd $TEST_DIR
    for item in $CLIENTS; do
        IFS=: read network client ip is_gateway <<< $item
        [[ -z $ip ]] && continue
        sed  -i networks/$network/$client/wg0.conf \
             -e "/Endpoint/ s/=.*:/= $wg_server_ip:/"
    done
}

hl_bash() {
    highlight -S bash -O xterm256 $1
}

hl_conf() {
    highlight -S ini -O xterm256 $1
}

##################### start #####################

echo "TITLE='$TITLE'" | hl_bash
echo "DESCRIPTION='$DESCRIPTION'" | hl_bash
echo "NETWORKS='$NETWORKS'" | hl_bash
echo "CLIENTS='$CLIENTS'" | hl_bash

# start verbose mode
set -o verbose

## Press [Enter] to continue

:; initialize_test_environment
