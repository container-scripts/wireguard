#!/bin/bash -i

TITLE="Accessing the internet from a WG client"
DESCRIPTION="
This test is related to the usecase#1, where a WG client can access
the internet through a secure connection to the WG server.
"
NETWORKS="
    WG:172.24.0.0/16
    LAN1:172.25.0.0/16
"
CLIENTS="
    LAN1:client1:192.168.100.1
"

source $(dirname $0)/lib.sh

:; cd $TEST_DIR/networks/WG/server/

:; hl_bash settings.sh    # check the settings

:; setup_config_files    # create config files

:; hl_conf wg0.conf    # server config

:; hl_conf clients/client1.conf    # client1 config

:; setup_local_endpoint    # set Endpoint to the local ip of the server

#
# Go to client1
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; ls

:; hl_conf wg0.conf

#
# Check network configuration
#
##

:; cs exec ip addr

:; cs exec ip route

#
# Start up interface wg0
#
##

:; cs exec wg-quick up wg0

:; cs exec ip addr

:; cs exec ip route

#
# Test connection to the WG server
#
##

:; cs exec ping -c3 10.100.100.1

#
# Test connection to the internet
#
##

:; cs exec ping -c3 8.8.8.8

:; cs exec traceroute 8.8.8.8

#
# Shut down the wg0 interface
#
##

:; cs exec wg-quick down wg0

:; cs exec ip addr

:; cs exec ip route

#
# Destroy the test environment
#
##

:; cleanup_test_environment
