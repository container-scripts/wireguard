#!/bin/bash -i

TITLE="Accessing clients from a cloud server"
DESCRIPTION="
This test is related to usecase#4, where a containerized application
on the cloud, which is located on the same container network as the
WG server, can access clients that don\'t have a public IP.
At the same time, clients can access the application, but not each-other.
"
NETWORKS="
    WG:172.24.0.0/16
    LAN1:172.25.0.0/16
    LAN2:172.26.0.0/16
"
CLIENTS="
    LAN1:client1:192.168.100.1:gateway
    LAN2:client2:192.168.100.2
    WG:client3
    LAN1:client4
"

source $(dirname $0)/lib.sh

#
# Step 1
# Make sure that ROUTED_NETWORKS includes '172.24.0.0/16' (the WG server network),
# 'ALLOW_INTERNET_ACCESS=no', 'CLIENT_TO_CLIENT=no', and 'KEEPALIVE_PERIOD=25'
#
##

:; cd $TEST_DIR/networks/WG/server/

:; sed -i settings.sh -e '/ROUTED_NETWORKS/ c ROUTED_NETWORKS="10.100.100.1, 172.24.0.0/16"'

:; sed -i settings.sh -e '/ALLOW_INTERNET_ACCESS/ c ALLOW_INTERNET_ACCESS=no'

:; sed -i settings.sh -e '/CLIENT_TO_CLIENT/ c CLIENT_TO_CLIENT=no'

:; sed -i settings.sh -e '/KEEPALIVE_PERIOD/ c KEEPALIVE_PERIOD=25'

:; hl_bash settings.sh

#
# Step 2
# Generate and setup configuration files
#
##

:; setup_config_files

:; setup_local_endpoint

#
# Step 3
# Start up WG connections on LAN1/client1 and LAN2/client2.
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec wg-quick up wg0

:; cs exec ping -c3 10.100.100.1

:; cd $TEST_DIR/networks/LAN2/client2/

:; cs exec wg-quick up wg0

:; cs exec ping -c3 10.100.100.1

:; cs exec ping -c3 192.168.100.1

: "We cannot ping client1 (192.168.100.1) because CLIENT_TO_CLIENT=no"

#
# Step 4
# From client3 ping client1 and client2.
#
##

:; cd $TEST_DIR/networks/WG/client3/

:; cs exec ip addr

:; cs exec ip route

:; cs exec ping -c3 192.168.100.1

:; cs exec traceroute -m5 192.168.100.1

: "The route to 192.168.100.0/24 is not going via the WG server"

## Add a route to 192.168.100.0/24 via the WG server

:; cs exec ip route add to 192.168.100.0/24 via 172.24.0.2

:; cs exec ip route

:; cs exec ping -c3 192.168.100.1

:; cs exec ping -c3 192.168.100.2

: "Now we can ping client1 and client2 from client3"

#
# Step 5
# Check that we can ping from client1 and client2 to client3
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec ping -c3 172.24.0.2

:; cs exec ping -c3 172.24.0.3

:; cd $TEST_DIR/networks/LAN2/client2/

:; cs exec ping -c3 172.24.0.2

:; cs exec ping -c3 172.24.0.3

: "We can ping not only to client3 (172.24.0.3)"
: "But also to the local IP of the WG server (172.24.0.2)"
: "which is on the same LAN (172.24.0.0/16)"

#
# Step 6
# On client3 add a route to LAN1 (172.25.0.0/16)
# via the WG/server (172.24.0.2).
#
##

:; cd $TEST_DIR/networks/WG/client3/

:; cs exec ip route

:; cs exec ping -c3 172.25.0.2

:; cs exec traceroute -m5 172.25.0.2

: "The route to 172.25.0.0/16 is not going via WG/server"

:; cs exec ip route add to 172.25.0.0/16 via 172.24.0.2

:; cs exec ip route

:; cs exec ping -c3 172.25.0.2

#
# Step 7
# Try to ping from LAN1/client4 to WG/client3.
#
##

:; cd $TEST_DIR/networks/LAN1/client4/

:; cs exec ip addr

:; cs exec ip route

:; cs exec ping -c3 172.24.0.3

:; cs exec traceroute -m5 172.24.0.3

: "The route to 172.24.0.0/16 is not going via LAN1/client1"

## Add a route to 172.24.0.0/16 via LAN1/client1 (172.25.0.2)

:; cs exec ip route add to 172.24.0.0/16 via 172.25.0.2

:; cs exec ip route

:; cs exec ping -c3 172.24.0.3

:; cs exec ping -c3 172.24.0.2

: "Now we can ping any client on the WG network (172.24.0.0/16)"

#
# Step 8
# Try to ping from WG/client3 to LAN1/client4.
#
##

:; cd $TEST_DIR/networks/WG/client3/

:; cs exec ip route

:; cs exec ping -c3 172.25.0.3

:; cs exec ping -c3 172.25.0.2

: "Now we can ping from WG/client3 to any client on LAN1 (172.25.0.0/16)"

:; cleanup_test_environment    # End
