# Testing WireGuard

Using podman and container-scripts we create virtual environments for
testing the different usecases of the WireGuard server (which are also
described on this doc: [WireGuard Usecases](/docs/wg-usecases.md)).

## Runing the test scripts

When you run a test script like this: `./test1.sh`, it actually
re-executes itself like this: `./play ./test1.sh`. So, you also need to
install the dependencies of `play`, which are `highlight` and
`expect`:

```
apt install highlight expect
```

In order to run the test script without `./play`, call it like this:
`./test1.sh noplay`. An alternative could also be `./test1.sh
autoplay` or `./test1.sh auto` (they are the same). In this case you
don't have to press **[Enter]** on each step because it will continue
automatically after a short delay.


## Test #1: Accessing the internet from a WG client

This test is related to the [Usecase
#1](/docs/wg-usecases.md#usecase-1-secure-connection-to-the-internet),
where a WG client can access the internet through a secure connection
to the WG server.

The testing environment is as shown in the diagram:

![Accessing the internet from a WG client](img/test1.svg)

The default settings of the server are OK, so we don't need to modify
them. After creating configuration files (for the client and for the
server), we go to `client1` and:
1. start up the interface `wg0`
1. test the connection to the WG server
1. test the connection to the internet


## Test #2: Two clients cannot ping each-other

This test is related to the [Usecase
#1](/docs/wg-usecases.md#usecase-1-secure-connection-to-the-internet),
where a WG client can access the internet through a secure connection
to the WG server. By default, two different clients cannot ping
each-other. The setting `CLIENT_TO_CLIENT` by default is commented out
(which means disabled).

![Two clients cannot ping each-other](img/test2.svg)

The default settings of the server are OK, so we don't need to modify
them. After creating configuration files:
1. start up `wg0` on client1
1. start up `wg0` on client2
1. try to ping from client1 to client2
1. try to ping from client2 to client1


## Test #3: Two clients can ping each-other

This test is related to a customized version of [Usecase
#1](/docs/wg-usecases.md#usecase-1-secure-connection-to-the-internet),
where WG clients can also ping each-other, besides accessing the
internet through the WG server. This requires the setting
`CLIENT_TO_CLIENT=yes` (enabled).

![Two clients can ping each-other](img/test3.svg)

1. set `CLIENT_TO_CLIENT=yes` on the server
1. generate configuration files
1. start up `wg0` on client1
1. start up `wg0` on client2
1. ping from client1 to client2
1. ping from client2 to client1


## Test #4: Virtual private LAN

This test is related to the [Usecase
#2](/docs/wg-usecases.md#usecase-2-virtual-private-lan), where clients
can access each-other, but cannot access the internet through the WG
interface (because of the setting `ALLOW_INTERNET_ACCESS=no`).

![Virtual private LAN](img/test4.svg)

1. settings:
   ```bash
   ROUTED_NETWORKS="10.100.100.1, 192.168.100.0/24"
   ALLOW_INTERNET_ACCESS=no
   CLIENT_TO_CLIENT=yes
   KEEPALIVE_PERIOD=25
   ```
1. generate client config files
1. start up `wg0` on client1 and check that the client does not access
   the internet through the WG server
1. start up `wg0` on client2 and check that the client does not access
   the internet through the WG server
1. ping from client1 to client2
1. ping from client2 to client1
1. set `AllowedIPs = 0.0.0.0/0` on the configuration of client1 and
   make sure that it still cannot access the internet through the WG
   server

## Test #5: Routing between two LANs

This test is related to the [Usecase
#3](/docs/wg-usecases.md#usecase-3-routing-between-remote-private-lans),
where clients on two different private LANs, can access each-other
through the WG network.

![Routing between two LANs](img/test5.svg)

1. settings:
   ```bash
   ROUTED_NETWORKS="10.100.100.1/32, 192.168.100.0/24, 172.25.0.0/16, 172.26.0.0/16"
   ALLOW_INTERNET_ACCESS=no
   CLIENT_TO_CLIENT=yes
   KEEPALIVE_PERIOD=25
   ```
1. generate client config files
1. start up WG interfaces on client1 and client2
1. on client3 add routes to 172.26.0.0/16 and 192.168.100.0/24 via
   client1
1. on client4 add routes to 172.26.0.0/16 and 192.168.100.0/24 via
   client2
1. check that client3 can ping to client2 and client4
1. check that client4 can ping to client1 and client3


## Test #6: Accessing clients from a cloud server

This test is related to [Usecase
#4](/docs/wg-usecases.md#usecase-4-accessing-clients-from-a-cloud-server),
where a containerized application on the cloud, which is located on
the same podman network as the WG server, can access clients that
don't have a public IP. At the same time, clients can access the
application, but not each-other.

![Accessing clients from a cloud server](img/test6.svg)

1. settings:
   ```bash
   ROUTED_NETWORKS="10.100.100.1, 172.24.0.0/16"
   ALLOW_INTERNET_ACCESS=no
   CLIENT_TO_CLIENT=no
   KEEPALIVE_PERIOD=25
   ```
1. generate client config files
1. start up WG interfaces on client1 and client2
1. on client3 add a route to 192.168.100.0/24 via WG server and ping client1 and client2
1. check that we can ping from client1 and client2 to client3
1. on client3 add a route to 172.25.0.0/16 via WG server
1. on client4 add a route to 172.24.0.0/16 via client1 and ping client3
1. check that we can ping from client3 to client4
