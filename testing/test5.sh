#!/bin/bash -i

TITLE="Routing between two LANs"
DESCRIPTION="
This test is related to the usecase#3, where clients on two different
private LANs, can access each-other through the WG network.
"
NETWORKS="
    WG:172.24.0.0/16
    LAN1:172.25.0.0/16
    LAN2:172.26.0.0/16
"
CLIENTS="
    LAN1:client1:192.168.100.1:gateway
    LAN2:client2:192.168.100.2:gateway
    LAN1:client3
    LAN2:client4
"

source $(dirname $0)/lib.sh

#
# Step 1
# Make sure that ALLOW_INTERNET_ACCESS is set to 'no' on the server settings.sh
# CLIENT_TO_CLIENT is set to 'yes', KEEPALIVE_PERIOD=25, and ROUTED_NETWORKS
# includes the LANs that will be connected: 172.25.0.0/16 and 172.26.0.0/16
#
##

:; cd $TEST_DIR/networks/WG/server/

:; sed -i settings.sh -e '/ALLOW_INTERNET_ACCESS/ c ALLOW_INTERNET_ACCESS=no'

:; sed -i settings.sh -e '/CLIENT_TO_CLIENT/ c CLIENT_TO_CLIENT=yes'

:; sed -i settings.sh -e '/KEEPALIVE_PERIOD/ c KEEPALIVE_PERIOD=25'

:; sed -i settings.sh -e '/ROUTED_NETWORKS/ c ROUTED_NETWORKS="10.100.100.1/32, 192.168.100.0/24, 172.25.0.0/16, 172.26.0.0/16"'

:; hl_bash settings.sh

#
# Step 2
# Generate client configurations and copy them to client1 and client2.
#
##

:; setup_config_files

:; setup_local_endpoint

#
# Step 3
# Start up WG connections on LAN1/client1 and LAN2/client2 and check
# that they work
#
##

:; cd $TEST_DIR/networks/LAN1/client1/

:; cs exec wg-quick up wg0

:; cs exec ip addr

:; cs exec ping -c3 10.100.100.1

:; cd $TEST_DIR/networks/LAN2/client2/

:; cs exec wg-quick up wg0

:; cs exec ip addr

:; cs exec ping -c3 10.100.100.1

## Ping client1 from client2

:; cs exec ping -c3 192.168.100.1

#
# Step 4
# Set up routing on LAN1/client3 (which has no WG connection)
# to route networks 172.26.0.0/16 and 192.168.100.0/24 through
# LAN1/client1 (IP: 172.25.0.2)
#
##

:; cd $TEST_DIR/networks/LAN1/client3/

:; cs exec ip addr

:; cs exec ip route

:; cs exec ip route add to 172.26.0.0/16 via 172.25.0.2

:; cs exec ip route add to 192.168.100.0/24 via 172.25.0.2

:; cs exec ip route

#
# Step 5
# Set up routing on LAN2/client4 (which has no WG connection)
# to route networks 172.25.0.0/16 and 192.168.100.0/24 through
# LAN2/client2 (IP: 172.26.0.2)
#
##

:; cd $TEST_DIR/networks/LAN2/client4/

:; cs exec ip addr

:; cs exec ip route

:; cs exec ip route add to 172.25.0.0/16 via 172.26.0.2

:; cs exec ip route add to 192.168.100.0/24 via 172.26.0.2

:; cs exec ip route

#
# Step 6
# Check the connectivity from LAN1/client3
#
##

:; cd $TEST_DIR/networks/LAN1/client3/

:; cs exec ip addr

:; cs exec ip route

:; cs exec ping -c3 192.168.100.2    # Ping LAN2/client2 (WG client)

:; cs exec ping -c3 172.26.0.2

:; cs exec ping -c3 172.26.0.3    # Ping LAN2/client4 (non-WG client)

:; cs exec traceroute 172.26.0.3

:; cs exec traceroute 8.8.8.8

: "Connection to internet is through the normal route"

#
# Step 7
# Try the same thing from LAN2/client4
#
##

:; cd $TEST_DIR/networks/LAN2/client4/

:; cs exec ip addr

:; cs exec ip route

:; cs exec ping -c3 192.168.100.1    # Ping LAN1/client1 (WG client)

:; cs exec ping -c3 172.25.0.2

:; cs exec ping -c3 172.25.0.3    # Ping LAN1/client3 (non-WG client)

:; cs exec traceroute 172.25.0.3

:; cs exec traceroute 8.8.8.8

: "Connection to internet is through the normal route"

:; cleanup_test_environment    # End
