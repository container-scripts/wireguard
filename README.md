# WireGuard scripts

## Installation

- First install `cs`: https://gitlab.com/container-scripts/cs#installation

- Then get the scripts: `cs pull wireguard`

- Create a directory for the container: `cs init wireguard @wireguard`

- Fix the settings: `cd /var/cs/wireguard/ ; vim settings.sh` <br/>
  Read also: [docs/wg-usecases.md](docs/wg-usecases.md)

- Make the container: `cs make`

## Usage

### On the server

1. Add a couple of clients:

   ```
   cs client add test1 192.168.10.2
   cs client ls
   cs client add test2 10.10.0.3
   cs client add test3 10.10.10.11
   cs client ls
   ls clients/
   ```

1. Delete one of them:

   ```
   cs client delete test1
   cs client ls
   ls clients/
   ```

1. Share a client config file:

   - Using QR code:
   
     ```
     cs share qr test2
     ```

     Scan it from Android or iPhone.

   - Using Tor:
   
     ```
     cs share tor test2
     ```

     Use a Tor Browser on the client side to download the
     configuration file.

   - Using www:
   
     ```
     cs share www test2
     cs share www test3
     ls www/
     ```

     On the client side use a command like this to get the config
     file:

     ```
     wget --no-check-certificate -O test3.conf \
	  https://11.12.13.14:4343/clients/test3.conf.HjamzWEpWW6z4LT
     ```

     To stop sharing by www run:

     ```
     cs share www stop
     ls www/
     ```

### On the client

1. Test the configuration file:

   ```
   apt install wireguard
   wg-quick up ./test2.conf
   
   ip addr
   ping 8.8.8.8
   traceroute 8.8.8.8
   curl ifconfig.co
   
   wg-quick down ./test2.conf
   ```

   **NB:** If your client is a RaspberryPi, you also have to install
   `raspberrypi-kernel-headers` and then reboot:

   ```
   apt install raspberrypi-kernel-headers
   reboot
   ```

1. Start the VPN connection as a service:

   ```
   mv test2.conf /etc/wireguard/wg0.conf
   systemctl enable wg-quick@wg0
   systemctl start wg-quick@wg0
   
   systemctl status wg-quick@wg0
   ip addr
   ping 8.8.8.8
   traceroute 8.8.8.8
   curl ifconfig.co
   ```

## Other commands

```
cs stop
cs start
cs shell
cs help
```
