include(focal)

### install wireguard
RUN apt install --yes iptables iproute2 wireguard

### install apache2 and qrencode
RUN apt install --yes apache2 qrencode

### install onionshare
RUN apt install --yes software-properties-common && \
    add-apt-repository ppa:micahflee/ppa && \
    apt install -y onionshare

RUN systemctl disable tor apache2

