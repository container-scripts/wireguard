cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    orig_cmd_create \
        --cap-add net_admin \
        --cap-add sys_module \
        --sysctl net.ipv4.ip_forward=1 \
        --publish $WG_PORT:51820/udp \
        --publish $HTTPS_PORT:443 \
        "$@"    # accept additional options, e.g.: -p 2201:22
}
