cmd_share_help() {
    cat <<_EOF
    cs share qr <client_name>
    cs share tor <client_name>

    cs share www <client_name>
    cs share www stop

_EOF
}

cmd_share() {
    local cmd=$1
    shift
    case $cmd in
        qr|tor)
            # get the client name
            local client_name=$1
            [[ -z $client_name ]] && fail "Usage:\n$(cmd_share_help)"
            [[ -f clients/$client_name.conf ]] \
                || fail "No config file for '$client_name'. Check with 'ls clients/'"
            # share the config file
            _share_$cmd $client_name
            ;;
        www)
            _share_www "$@"
            ;;
        *)
            fail "Usage:\n$(cmd_share_help)"
            ;;
    esac
}

_share_qr() {
    cs exec \
       bash -c "cat /host/clients/$client_name.conf | qrencode -t utf8"
}

_share_tor() {
    cs exec systemctl start tor
    cs exec onionshare /host/clients/$client_name.conf
    cs exec systemctl stop tor
}

_share_www() {
    local client_name=$1
    [[ -z $client_name ]] && fail "Usage:\n$(cmd_share_help)"
    
    if [[ $client_name == 'stop' ]]; then
        cs exec systemctl stop apache2
        rm -rf www/
    else
        [[ -f clients/$client_name.conf ]] \
            || fail "No config file for '$client_name'. Check with 'ls clients/'"

        # start apache2
        mkdir -p www
        cs exec systemctl start apache2

        # make the config file accessible through www
        local key=$(tr -cd '[:alnum:]' < /dev/urandom | head -c15)
        cp -f clients/$client_name.conf www/$client_name.conf.$key
        cat clients/$client_name.conf | qrencode -o www/$client_name.png.$key
        tree ./www/
        local public_ip=$(get_public_ip)
        cat <<EOF

wget --no-check-certificate -O ${client_name}.conf \\
     https://${public_ip}:${HTTPS_PORT}/clients/${client_name}.conf.$key
wget --no-check-certificate -O ${client_name}.png \\
     https://${public_ip}:${HTTPS_PORT}/clients/${client_name}.png.$key

EOF
    fi
}
