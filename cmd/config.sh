cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    cs inject ubuntu-fixes.sh
    #cs inject ssmtp.sh
    #cs inject logwatch.sh
    cs inject apache2-redirect-to-https.sh

    cs inject wireguard.sh
    cs inject www.sh
}
