cmd_client_help() {
    cat <<_EOF
    cs client add <client_name> <client_ip> [<client_lan>]
    cs client ls
    cs client del <client_name>

_EOF
}

cmd_client() {
    local cmd=$1 ; shift
    case $cmd in
        add|ls|del) _client_$cmd "$@" ;;
        *)          _client_fail      ;;
    esac
}

_client_fail() {
    fail "Usage:\n$(cmd_client_help)"
}

_client_add() {
    local client_name=$1
    local client_ip=$2
    local client_lan=$3
    [[ -z $client_name ]] && _client_fail
    [[ -z $client_ip ]] && _client_fail
    [[ -f clients/$client_name.conf ]] && fail "A client with name '$client_name' already exists"

    local ip_cnt=$(grep -c $client_ip wg0.conf)
    [[ $ip_cnt == '0' ]] || fail "Another client is using IP='$client_ip'"

    # Get the keys
    local server_private_key=$(cat wg0.conf | grep PrivateKey | sed 's/PrivateKey = //')
    local server_public_key=$(cs exec bash -c "echo $server_private_key | wg pubkey" | tr -d "\r")
    local client_private_key=$(cs exec wg genkey | tr -d "\r")
    local client_public_key=$(cs exec bash -c "echo $client_private_key | wg pubkey" | tr -d "\r")
    local pre_shared_key=$(cs exec wg genpsk | tr -d "\r")

    # Add the client as a peer to the server
    local allowed_ips="$client_ip"
    [[ -n $client_lan ]] && allowed_ips+=", $client_lan"
    cat <<EOF >> wg0.conf
### Client $client_name
[Peer]
PublicKey = $client_public_key
PresharedKey = $pre_shared_key
AllowedIPs = $allowed_ips

EOF

    # Create a config file for the client
    mkdir -p clients
    local public_ip=$(get_public_ip)
    local allowed_ips=${ROUTED_NETWORKS:-0.0.0.0/0}
    if [[ $allowed_ips != '0.0.0.0/0' && -n $client_lan ]]; then
        # Remove client_lan from the list of allowed_ips
        allowed_ips=$(
            echo $allowed_ips \
                | sed "s#$client_lan##" \
                | sed -e 's/, *$//' \
                      -e 's/^ *, *//' \
                      -e 's/, *,/,/'
                   )
    fi
    cat <<EOF > clients/$client_name.conf
[Interface]
PrivateKey = $client_private_key
Address = $client_ip
DNS = ${DNS_SERVERS:-176.103.130.130, 176.103.130.131}
# client_lan = $client_lan

[Peer]
PublicKey = $server_public_key
PresharedKey = $pre_shared_key
Endpoint = $public_ip:$WG_PORT
PersistentKeepalive = ${KEEPALIVE_PERIOD:-0}
AllowedIPs = $allowed_ips
EOF
    cat clients/$client_name.conf

    # restart the service
    cs exec systemctl restart wg-quick@wg0
}

_client_ls() {
    local file client_name client_ip
    for file in $(ls clients/*.conf); do
        client_name=$(basename ${file%.conf})
        client_ip=$(cat $file | grep Address | sed 's/Address = //')
        client_lan=$(cat $file | grep client_lan | sed 's/# client_lan = //')
        echo -e "$client_name\t$client_ip\t$client_lan"
    done
}

_client_del() {
    local client_name=$1
    [[ -z $client_name ]] && _client_fail

    # remove the [Peer] block matching $client_name
    sed -i wg0.conf \
        -e "/^### Client $client_name\$/,/^$/d"

    # remove the config file
    rm -f clients/$client_name.conf

    # restart wireguard
    cs exec systemctl restart wg-quick@wg0
}
