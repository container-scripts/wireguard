cmd_setup_help() {
    cat <<'EOF'
Usage:
    cs setup gw [<client_name>:<client_ip> ...]
        Each client uses the WG server as a gateway to internet.
        However the clients cannot access each-other.

    cs setup lan <subnet> [<client_name> ...]
        Each client can access the other clients, but they cannot
        access the internet through the WG server.

    cs setup router [<client_name>:<client_ip>:<client_lan> ...]
        Each client can be used as a router by the machines on its LAN
        to access the machines on the other LANs.
        This is like an extended 'lan' setup, where not only the clients
        themselves can access each-other, but also the machines on their
        LANs can access each other. However each machine has to add
        routes to the LANs that it needs to access. These routes go via
        the WG client.

Examples:
    cs setup gw \
        client1:10.10.10.1 \
        client2:10.10.10.2 \
        client3:10.10.10.3

    cs setup lan \
        10.10.10.0/16 \
        client1 client2 client3

    cs setup router \
        client1:10.0.0.1:192.168.11.0/24 \
        client2:10.0.0.2:192.168.12.0/24 \
        client3:10.0.0.3:192.168.13.0/24

EOF
}

cmd_setup() {
    local cmd=$1 ; shift
    case $cmd in
        gw|lan|router)
            _setup_$cmd "$@"
            ;;
        *)
            cmd_setup_help
            ;;
    esac
}

_setup_gw() {
    # make sure that settings have correct values
    sed -i settings.sh \
        -e '/ROUTED_NETWORKS/ c ROUTED_NETWORKS="0.0.0.0/0"' \
        -e '/ALLOW_INTERNET_ACCESS/ c ALLOW_INTERNET_ACCESS=yes' \
        -e 's/^CLIENT_TO_CLIENT/#CLIENT_TO_CLIENT/' \
        -e 's/^KEEPALIVE_PERIOD/#KEEPALIVE_PERIOD/'
    echo "===== settings.sh ====="
    grep -E "(ROUTED_NETWORKS|ALLOW_INTERNET_ACCESS|CLIENT_TO_CLIENT|KEEPALIVE_PERIOD)" \
         --color settings.sh

    # add client configurations
    local args="$@"    
    local client_name client_ip
    for arg in $args; do
        IFS=: read client_name client_ip <<< $arg
        echo -e "\n===== cs client add $client_name $client_ip ====="
        cs client add $client_name $client_ip
        shift
    done

    # restart the service
    cs exec systemctl restart wg-quick@wg0
}

_setup_lan() {
    # make sure that settings have correct values
    local subnet=$1 ; shift
    sed -i settings.sh \
        -e "/ROUTED_NETWORKS/ c ROUTED_NETWORKS='$subnet'" \
        -e '/ALLOW_INTERNET_ACCESS/ c ALLOW_INTERNET_ACCESS=no' \
        -e '/CLIENT_TO_CLIENT/ c CLIENT_TO_CLIENT=yes' \
        -e '/KEEPALIVE_PERIOD/ c KEEPALIVE_PERIOD=25'
    echo "===== settings.sh ====="
    grep -E "(ROUTED_NETWORKS|ALLOW_INTERNET_ACCESS|CLIENT_TO_CLIENT|KEEPALIVE_PERIOD)" \
         --color settings.sh

    # add client configurations
    local args="$@"    
    local client_name client_ip i=1
    for client_name in $args; do
        client_ip=${subnet%.*}.$i ; let i=i+1
        echo -e "\n===== cs client add $client_name $client_ip ====="
        cs client add $client_name $client_ip
    done

    # restart the service
    cs exec systemctl restart wg-quick@wg0
}

_setup_router() {
    local args="$@"
    
    # get all the networks
    local networks=''
    local client_name client_ip client_lan
    for arg in $args; do
        IFS=: read client_name client_ip client_lan <<< $arg
        networks+="$client_lan, "
    done
    networks=${networks%, }
    
    # make sure that settings have correct values
    sed -i settings.sh \
        -e "/ROUTED_NETWORKS/ c ROUTED_NETWORKS='$networks'" \
        -e '/ALLOW_INTERNET_ACCESS/ c ALLOW_INTERNET_ACCESS=no' \
        -e '/CLIENT_TO_CLIENT/ c CLIENT_TO_CLIENT=yes' \
        -e '/KEEPALIVE_PERIOD/ c KEEPALIVE_PERIOD=25'
    echo "===== settings.sh ====="
    grep -E "(ROUTED_NETWORKS|ALLOW_INTERNET_ACCESS|CLIENT_TO_CLIENT|KEEPALIVE_PERIOD)" \
         --color settings.sh

    # add client configurations
    for arg in $args; do
        IFS=: read client_name client_ip client_lan <<< $arg
        echo -e "\n===== cs client add $client_name $client_ip $client_lan ====="
        cs client add $client_name $client_ip $client_lan
    done

    # restart the service
    cs exec systemctl restart wg-quick@wg0
}
